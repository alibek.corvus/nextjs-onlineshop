import React, { FC } from 'react'
import { clsx } from 'clsx'
import styles from './Button.module.scss'

interface IProps {
  label: string
  isAlignLeft: boolean
  variant: ButtonVariant
  disabled: boolean
  buttonType: ButtonType
  isLoading: boolean
  buttonSize: ButtonSize
  submit: (props: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {}
}

const Button: FC<IProps> = ({
  buttonSize = ButtonSize.DEFAULT,
  submit,
  buttonType = 'submit',
  variant = ButtonVariant.REGULAR,
  isLoading,
  disabled,
  label,
  isAlignLeft,
}) => {
  return (
    <button
      onClick={submit}
      disabled={disabled}
      type={buttonType}
      className={clsx({
        [styles.buttonLoading]: isLoading,
        [styles.buttonDisabled]: disabled,
        [styles.buttonAlignLeft]: isAlignLeft,
        [styles.buttonSmallSize]: buttonSize === ButtonSize.SMALL,
        [styles.buttonRounded]: variant === ButtonVariant.ROUNDED,
        [styles.buttonBordered]: variant === ButtonVariant.BORDERED,
        [styles.buttonUnderlined]: variant === ButtonVariant.UNDERLINED,
        [styles.buttonContentOnly]: variant === ButtonVariant.CONTENT_ONLY,
      })}
    >
      {label}
    </button>
  )
}

export default Button

export enum ButtonVariant {
  REGULAR,
  CONTENT_ONLY,
  BORDERED,
  UNDERLINED,
  ROUNDED,
}

export type ButtonType = 'button' | 'submit' | 'reset'

export enum ButtonSize {
  DEFAULT,
  SMALL,
}
