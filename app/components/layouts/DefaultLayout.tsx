import React, { FC } from 'react'

interface IProps {
  children: React.ReactNode
}

const DefaultLayout: FC<IProps> = ({ children }) => {
  return <div>{children}</div>
}

export default DefaultLayout
